require "application_system_test_case"

class DoacaosTest < ApplicationSystemTestCase
  setup do
    @doacao = doacaos(:one)
  end

  test "visiting the index" do
    visit doacaos_url
    assert_selector "h1", text: "Doacaos"
  end

  test "creating a Doacao" do
    visit doacaos_url
    click_on "New Doacao"

    fill_in "Comentario", with: @doacao.comentario
    fill_in "Cpf", with: @doacao.cpf
    fill_in "Idade", with: @doacao.idade
    fill_in "Nome", with: @doacao.nome
    fill_in "Raca", with: @doacao.raca
    click_on "Create Doacao"

    assert_text "Doacao was successfully created"
    click_on "Back"
  end

  test "updating a Doacao" do
    visit doacaos_url
    click_on "Edit", match: :first

    fill_in "Comentario", with: @doacao.comentario
    fill_in "Cpf", with: @doacao.cpf
    fill_in "Idade", with: @doacao.idade
    fill_in "Nome", with: @doacao.nome
    fill_in "Raca", with: @doacao.raca
    click_on "Update Doacao"

    assert_text "Doacao was successfully updated"
    click_on "Back"
  end

  test "destroying a Doacao" do
    visit doacaos_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Doacao was successfully destroyed"
  end
end
