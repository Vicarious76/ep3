require 'test_helper'

class DoacaosControllerTest < ActionDispatch::IntegrationTest
  setup do
    @doacao = doacaos(:one)
  end

  test "should get index" do
    get doacaos_url
    assert_response :success
  end

  test "should get new" do
    get new_doacao_url
    assert_response :success
  end

  test "should create doacao" do
    assert_difference('Doacao.count') do
      post doacaos_url, params: { doacao: { comentario: @doacao.comentario, cpf: @doacao.cpf, idade: @doacao.idade, nome: @doacao.nome, raca: @doacao.raca } }
    end

    assert_redirected_to doacao_url(Doacao.last)
  end

  test "should show doacao" do
    get doacao_url(@doacao)
    assert_response :success
  end

  test "should get edit" do
    get edit_doacao_url(@doacao)
    assert_response :success
  end

  test "should update doacao" do
    patch doacao_url(@doacao), params: { doacao: { comentario: @doacao.comentario, cpf: @doacao.cpf, idade: @doacao.idade, nome: @doacao.nome, raca: @doacao.raca } }
    assert_redirected_to doacao_url(@doacao)
  end

  test "should destroy doacao" do
    assert_difference('Doacao.count', -1) do
      delete doacao_url(@doacao)
    end

    assert_redirected_to doacaos_url
  end
end
