json.extract! adocao, :id, :nome, :idade, :cpf, :endereco, :comentario, :created_at, :updated_at
json.url adocao_url(adocao, format: :json)
