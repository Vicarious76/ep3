json.extract! doacao, :id, :nome, :cpf, :idade, :comentario, :raca, :created_at, :updated_at
json.url doacao_url(doacao, format: :json)
