class CreateAdocaos < ActiveRecord::Migration[6.0]
  def change
    create_table :adocaos do |t|
      t.string :nome
      t.string :idade
      t.string :cpf
      t.string :endereco
      t.text :comentario

      t.timestamps
    end
  end
end
