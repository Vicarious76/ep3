class CreateDoacaos < ActiveRecord::Migration[6.0]
  def change
    create_table :doacaos do |t|
      t.string :nome
      t.string :cpf
      t.string :idade
      t.text :comentario
      t.string :raca

      t.timestamps
    end
  end
end
